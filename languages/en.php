<?php
$english = array(
	'simplepie:widget' => 'RSS Feed',
	'simplepie:description' => 'Add an external blog to your profile',
	'simplepie:notset' => 'Feed url is not set',
	'simplepie:notfound' => 'Cannot find feed. Check the feed url.',
	'simplepie:feed_url' => 'Feed URL',
	'simplepie:num_items' => 'Number of items',
	'simplepie:excerpt' => 'Include excerpt',
	'simplepie:post_date' => 'Include post date',
	'simplepie:postedon' => 'Posted on',
	'simplepie:invalid_url' => 'Invalid url, copy it from the navigation bar please',
);
add_translation("en", $english);
