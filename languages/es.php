<?php
$spanish = array(
        'simplepie:widget' => 'Enlace RSS',
        'simplepie:description' => 'Agregar un blog externo',
        'simplepie:notset' => 'Enlace RSS no configurado',
        'simplepie:notfound' => 'no se encontro el feed. Revisa el feed url.',
        'simplepie:feed_url' => 'Feed URL',
        'simplepie:num_items' => 'Numero de items',
        'simplepie:excerpt' => 'Incluir contenido',
        'simplepie:post_date' => 'Incluir fecha del post',
        'simplepie:postedon' => 'Posted on',
        'simplepie:invalid_url' => 'Url invalida, copiela desde la barra del navegador por favor',
);
add_translation("en", $english);
